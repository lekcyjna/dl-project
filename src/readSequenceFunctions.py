import numpy as np
import random

allCodons=frozenset({'AGC', 'CAT', 'TCG', 'GAA', 'TGG', 'TGA', 'GAT', 'CCC', 'CTT', 'CGG', 'GCT', 'TGT', 'AGA', 'TAA', 'GTA', 'CTC', 'AAC', 'CAC', 'AAA', 'CGA', 'TGC', 'TTA', 'TTG', 'ATC', 'GAG', 'TCT', 'GCA', 'GTC', 'AAT', 'GGA', 'CCA', 'TTT', 'TCA', 'GGT', 'TAT', 'TAC', 'GAC', 'GGC', 'AAG', 'CTG', 'GCG', 'AGG', 'TAG', 'GCC', 'CGT', 'ACA', 'TCC', 'TTC', 'ATG', 'CGC', 'ATA', 'GTG', 'CTA', 'GTT', 'GGG', 'ACC', 'CCG', 'AGT', 'CAA', 'CAG', 'ATT', 'CCT', 'ACT', 'ACG'})
stopCodons=frozenset({"TAG","TGA","TAA"})

def createWeightsDict(matrix):
    '''Take ndarray which describe transition matrix.'''
    N=["A","T","C","G"]
    weights={}
    for n1 in range(4):
        for n2 in range(4):
            for n3 in range(4):
                s=0
                #TAG
                s+=matrix[n1,1]*matrix[n2,0]*matrix[n3,3]
                #TGA
                s+=matrix[n1,1]*matrix[n2,3]*matrix[n3,0]
                #TAA
                s+=matrix[n1,1]*matrix[n2,0]*matrix[n3,0]
                codon=N[n1]+N[n2]+N[n3]
                weights[codon]=s
    return weights


class Coding():
    def __init__(self,n=1, mutationMatrix=None, maxNumberOfGroups=None):
        self.type=n;
        self.coding=None
        self.stopCodonsSymbol=None
        self.numberOfSymbols=-1
        self.threshold=None
        self.changeCoding(n, mutationMatrix, maxNumberOfGroups)

    def changeCoding(self,n, mutationMatrix=None,maxNumberOfGroups=None):
        '''Take number of coding and chage patterns'''
        if n==1:
            self.createCoding1()
        elif n==2:
            self.createCoding2()
        elif n==3:
            self.createCoding3(mutationMatrix)
        elif n==4:
            self.createCoding4(mutationMatrix)
        elif n==5:
            self.createCoding5(mutationMatrix, maxNumberOfGroups)
        else:
            raise RuntimeError("Wrong coding number.")
        self.numberOfSymbols=len(self.coding)

    def createCoding1(self):
        singleDangerousCodons=frozenset({"AAA","AAG","AGA","CAA","CAG","CGA","GAA","GAG","GGA","TCG","TGC","TGT","TTG"})
        doubleDangerousCodons=frozenset({"TAC","TAT","TCA","TGG","TTA"})
        safeCodons=frozenset({"AGC","CGC","CAT","CGG","GTA","GCA","GAT","CCG","ACT","ATG","TTT",
                    "CCC","GTC","GCG","CTT","ATA","CTA","AGT","GAC","CCT","ATT","GGG",
                    "CCA","CGT","GGC","CTC","CTG","TCT","AAC","GTT","ACC","TCC","GTG",
                    "ACG","AAT","ATC","CAC","GCT","GGT","GCC","AGG","ACA","TTC"})
        stopCodons=frozenset({"TAG","TGA","TAA"})

        self.coding={safeCodons:0, singleDangerousCodons:1,doubleDangerousCodons:2,stopCodons:3}
        self.stopCodonsSymbol=3

    def createCoding2(self):
        type0=frozenset(["TAA","TAG","TGA"])
        type1=frozenset(["TAT","TAC","TGT","TGC","TGG",
                        "TCG","TTG","TTA","TCA","CAA",
                        "CAG","CGA","AAA","AAG","AGA",
                        "GAA","GAG","GGC"])
        type2=frozenset(["TTT","TTC","TCT","TCC","CTA",
                        "CTG","CCA","CCG","CAT","CAC",
                        "CGT","CGC","CGG","ATA","ATG",
                        "ACA","ACG","AAT","AAC","AGT",
                        "AGC","AGG","GTA","GTG","GCA",
                        "GCG","GAT","GAC","GGT","GGA",
                        "GGG"])
        type3=frozenset(["CTT","CTC","CCT","CCC","ATT",
                        "ATC","ACT","ACC","GTT","GTC",
                        "GCT","GCC"])

        self.coding={type0:0, type1:1,type2:2,type3:3}
        self.stopCodonsSymbol=0

    def createCoding3(self, mutationMatrix):
        if mutationMatrix is None:
            raise RuntimeError("Coding 3 not received mutation matrix.")
        self.coding={}
        weights=createWeightsDict(mutationMatrix)
        for codon in weights:
            if codon not in stopCodons:
                self.coding[frozenset([codon])]=weights[codon]
        self.coding[stopCodons]=1
        self.stopCodonsSymbol=1

    def createCoding4(self, mutationMatrix):
        if mutationMatrix is None:
            raise RuntimeError("Coding 4 not received mutation matrix.")
        self.coding={}
        weights=createWeightsDict(mutationMatrix)

        bounduaries=[0.002, 0.007, 0.02, 0.05, 0.2, 1]
        groups=[[] for i in range(len(bounduaries))]

        for codon in weights:
            for i in range(len(bounduaries)):
                if weights[codon]<bounduaries[i]:
                    (groups[i]).append(codon)
                    break
        i=0
        for group in groups:
            self.coding[frozenset(group)]=i
            i+=1
        self.stopCodonsSymbol=len(bounduaries)-1

    def createCoding5(self, mutationMatrix, maxNumberOfGroups):
        def countGroups(rel, tres):
            count=1
            for r in rel:
                if r>tres:
                    count+=1
            return count

        if mutationMatrix is None or maxNumberOfGroups is None:
            raise RuntimeError("Coding 5 not received enough arguments. Mutation matrix: "+str(mutationMatrix)+" Group number: "+str(maxNumberOfGroups))
        if maxNumberOfGroups<1:
            raise RuntimeError("Attempt to create coding 5 with less than two groups (one for stops and one for rest).")
        #One additionaly group for stop codons
        maxNumberOfGroups+=1
        self.coding={}
        weights=createWeightsDict(mutationMatrix)

        for stop in stopCodons:
            weights[stop]=0.99

        wList=list(weights.values())
        wList.sort()
        relations=[wList[i]/wList[i-1] for i in range(1,len(wList))]

        ##############
        #binary search
        ##############

        start=1
        end=max(relations)

        while (start+1e-9)<end:
            mid=(start+end)/2
            C=countGroups(relations,mid)
            if C==maxNumberOfGroups:
                end=mid
                break
            if C>maxNumberOfGroups:
                start=mid
            else:
                end=mid

        threshold=end

        #Boundaries are first elements wich are not included to group
        bounduaries=[]
        for i in range(len(relations)):
            if relations[i]>threshold:
                #wList has offset 1 in comparasion to relations
                bounduaries.append(wList[i+1])

        bounduaries.append(max(relations)+1)

        groups=[[] for i in range(len(bounduaries))]

        for codon in weights:
            for i in range(len(bounduaries)):
                if weights[codon]<bounduaries[i]:
                    (groups[i]).append(codon)
                    break
        i=0
        for group in groups:
            self.coding[frozenset(group)]=i
            i+=1
        self.stopCodonsSymbol=len(bounduaries)-1
        self.threshold=threshold



def loadSequencesIntoDict(dictionary,file):
    '''Take handle file to fasta file and load all seqences from it into dictionary.'''
    #print(dictionary)
    for line in file:
        line=line.strip()
        #If line is empty
        if line==False:
            contine
        if line[0]==">":
            currentSequence=line[1:]
            if currentSequence in dictionary:
                #print(dictionary)
                raise RuntimeError()
            dictionary[currentSequence]=""
        else:
            line=line.replace(" ","")
            dictionary[currentSequence]=dictionary[currentSequence]+line


# ## Functions on sequences

def lenToThreeDivisable(sequence):
    remind=len(sequence)%3
    if remind==1:
        return ""
    if remind==2:
        return ""
    return sequence


def sequenceToTypeList(sequence):
    '''Take sequence with proper length (divisable by 3)
    and return list with type of each codon.'''
    typeList=[-1]*int(len(sequence)/3)
    for codonStart in range(0,len(sequence),3):
        codon=sequence[codonStart:codonStart+3]
        for codonGroup in usedCoding.coding:
            if codon in codonGroup:
                typeList[int(codonStart/3)]=usedCoding.coding[codonGroup]
                break
    return typeList


def removeStopTypeFromSeq(seq):
    '''Take sequence in form of string and remove all non-last stop codons,
    if there is no last codon then add "TAG".'''
    if len(seq)<3:
        return ""
    #Removel all last codons, which isn't last
    if seq[-3:] not in stopCodons:
        return ""
    while seq[-6:-3] in stopCodons:
        seq=seq[:-3]

    for i in range(0,len(seq)-3,3):
        if seq[i:i+3] in stopCodons:
            return ""
    return seq


def removeNonCorrectCodons(sequence):
    '''Take sequence and remove all non-corect codons.'''
    for codonStart in range(0,len(sequence),3):
        codon=sequence[codonStart:codonStart+3]
        if codon not in allCodons:
            return ""
    return sequence


# ## Functions on dictionary with sequences

def removeEmptyFromDictionary(seqDict):
    '''Take dictionary and remove all empty sequence.'''
    toRemove=[]
    for seqName in seqDict:
        if not seqDict[seqName]:
            toRemove.append(seqName)
    while len(toRemove)>0:
        del seqDict[toRemove[-1]]
        toRemove.pop()
    return seqDict


def cleanDictionary(seqDict):
    '''Each sequence in dictionary is shrinked to length
    which is divisable by three,non-correct codons are
    removed and empty sequences are removed.'''
    toRemove=[]
    for seqName in seqDict:
        seqDict[seqName]=lenToThreeDivisable(seqDict[seqName])
        seqDict[seqName]=removeNonCorrectCodons(seqDict[seqName])
    seqDict=removeEmptyFromDictionary(seqDict)
    return seqDict


def createDictionaryWithListsOfCodonsType(seqDict):
    '''Take dictionary with sequences and return
    dictionary with lists of codons type.'''
    listDict={}
    for seqName in seqDict:
        listDict[seqName]=sequenceToTypeList(seqDict[seqName])
    return listDict


def removeStopsFromDictionary(seqDict):
    '''Take dictionary with sequences in strings
    and remove all stops.'''
    for seqName in seqDict:
        seqDict[seqName]=removeStopTypeFromSeq(seqDict[seqName])


def convertListDictionaryIntoNdarrayDictionary(seqDict):
    for seqName in seqDict:
        seqDict[seqName]=np.array(seqDict[seqName])


def prepareDictionary(paths):
    """Construct dictionaries with sequences and coded sequences from fasta files"""
    seqDict={}
    for p in paths:
        file=open(p,"r")
        loadSequencesIntoDict(seqDict,file)
        file.close()
    seqDict=cleanDictionary(seqDict)
    removeStopsFromDictionary(seqDict)
    removeEmptyFromDictionary(seqDict)
    seqDictL=createDictionaryWithListsOfCodonsType(seqDict)
    convertListDictionaryIntoNdarrayDictionary(seqDictL)
    return seqDict, seqDictL

def traindAndTestDataset(paths, testSize=50, generator=None):
    """ Construct datasets base on fasta files.
    Arguments:
        paths - list of paths to fasta files
        testSize=50 - size of list with test sequences
        generator=None - numpy random number generator, which will be used to choose sequences to test list
    Return:
        seqDict - dictionary with names from fasta file as keys and list of nucleotides as values
        seqDictT -dictionary with names from fasta file as keys and list of coded codons as values
        trainList - list with sequences (coded codons) choosed to train list
        trainNamesList - list of names of sequences choosed to train list. Name i is name of sequence i from trainList
        testList - list with sequences (coded codons) choosed to test list
        testNamesList - list of names of sequences choosed to test list. Name i is name of sequence i from testList
    """
    seqDict,seqDictT=prepareDictionary(paths)

    seqList=list(seqDictT.values())
    seqNamesList=list(seqDictT.keys())

    if generator is not None:
        indTest=generator.choice(range(len(seqList)),size=testSize,replace=False)
    else:
        indTest=np.random.choice(range(len(seqList)),size=testSize,replace=False)
    indTrain=np.delete(np.arange(len(seqList)), indTest)

    testList=[seqList[i] for i in indTest]
    testNamesList=[seqNamesList[i] for i in indTest]
    trainList=[seqList[i] for i in indTrain]
    trainNamesList=[seqNamesList[i] for i in indTrain]
    return (seqDict,seqDictT,trainList,trainNamesList,testList,testNamesList)


