1. Dane

2. Metody
  - Prosta sieć LSTM - uczymy na sekwencjach sieć mającą stan ukryty długości d. Jako wynik uczenia traktujemy ostatni ukryty stan i redukujemy (warstwą/ami Liniowymi przykładowo) go tak, aby jego długość odpowiadała liczbie grup, jaką chcemy uzyskać. Jako funkcję celu chcemy maksymalizować aktywację najbardziej aktywnego nauronu w ostatniej warstwie.
  - Transformery - spróbujmy nauczyć transformery tab by znajdowały zanurzanie sekwencji w przestrzeni mniej wymiarowej. W tym celu można wykorzystać przykładowo model BERT.
    Otrzymanie wyniku:
    - redukcja zanurzenia przy pomocy sieci sekwencyjnej to rozmiaru o pożądanej wielkości i maksymalizacja aktywacji najbardziej aktywnego neuronu
    - zastosowanie autoencoderów
